﻿namespace Patterns.Prototype
{
    public interface iCopyable
    {
        iCopyable Copy();
    }
}
