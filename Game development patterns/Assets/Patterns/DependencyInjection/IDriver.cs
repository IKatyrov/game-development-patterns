﻿namespace Patterns.DependencyInjection
{
    public interface IDriver
    {
        void Control(Bike bike);
    }
}