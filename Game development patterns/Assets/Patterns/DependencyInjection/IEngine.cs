﻿namespace Patterns.DependencyInjection
{
    public interface IEngine
    {
        void StartEngine();
    }
}