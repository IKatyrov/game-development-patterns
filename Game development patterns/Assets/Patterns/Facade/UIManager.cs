﻿using UnityEngine;

namespace Patterns.Facade
{
    public class UIManager
    {
        public void DisplaySaveIcon()
        {
            Debug.Log("Displaying the save icon.");
        }
    }
}
