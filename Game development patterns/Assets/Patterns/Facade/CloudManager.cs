﻿using UnityEngine;

namespace Patterns.Facade
{
    public class CloudManager
    {
        public void UploadSaveGame(string playerData)
        {
            Debug.Log("Uploading save data.");
        }
    }
}
