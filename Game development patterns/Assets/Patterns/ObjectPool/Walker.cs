﻿using UnityEngine;

namespace Patterns.ObjectPool
{
    public class Walker : MonoBehaviour
    {
        public void Move()
        {
            // Zombie walks!
        }
    }
}
