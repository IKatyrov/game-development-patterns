﻿namespace Patterns.Strategy
{
    public class Torpedo : Missile
    {
        void Awake()
        {
            seekBehavior = new SeekWithSonar();
        }
    }
}
