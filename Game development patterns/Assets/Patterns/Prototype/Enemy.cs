﻿using UnityEngine;

namespace Patterns.Prototype
{
    public class Enemy : MonoBehaviour, iCopyable
    {
        public iCopyable Copy()
        {
            return Instantiate(this);
        }
    }
}